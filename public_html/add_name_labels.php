<?PHP

include ( "php/common.php" ) ;
ini_set('memory_limit','3500M');


$db = openDB ( 'wikidata' , 'wikidata' ) ;
$from_lang = $db->real_escape_string ( get_request ( 'from' , 'es,fr' ) ) ;
$missing_lang = $db->real_escape_string ( get_request ( 'to' , 'en' ) ) ;
$wdq = get_request ( 'wdq' , '' ) ;

print get_common_header ( '' , 'Names as labels' ) ;


print "
<form method='get'>
From language with labels <input type='text' name='from' value='$from_lang' />
to language without labels <input type='text' name='to' value='$missing_lang' />
<input type='submit' value='Do it!' name='doit' class='btn btn-primary' />
<br/>
<i>or</i> <input type='text' name='wdq' value='$wdq' placeholder='WDQ' />
</form>
" ;

if ( !isset($_REQUEST['doit']) ) exit ( 0 ) ;

$wiki = $from_lang . 'wiki' ;
if ( $wdq == '' ) $wdq = "claim[31:5]" ;
$url = $wdq_internal_url . "?q=" . urlencode ( $wdq ) ;
$json = json_decode ( file_get_contents ( $url ) ) ;


$q2label = array() ;

while ( count($json->items) > 0 ) {
	$tmp = array() ;
	while ( count($json->items) > 0 and count($tmp) < 150000 ) $tmp[] = array_pop ( $json->items ) ;
	$sql = "SELECT * FROM wb_terms t1 WHERE " ;

	if ( $from_lang != '' ) $sql .= " t1.term_language IN (" . "'" . implode ( "','" , explode ( ',' , $from_lang ) ) . "' ) AND " ;

	$sql .= " t1.term_entity_type='item' AND t1.term_type='label' AND t1.term_full_entity_id IN ('Q" . implode("','Q",$tmp) . "') AND NOT EXISTS (SELECT * FROM wb_terms t2 WHERE t1.term_full_entity_id=t2.term_full_entity_id AND t2.term_entity_type='item' AND t2.term_language='$missing_lang' AND t2.term_type='label')" ;

	$sql .= " ORDER BY term_full_entity_id" ;

	$last = '' ;
	$result = getSQL ( $db , $sql ) ;
	while($o = $result->fetch_object()){
		if ( $last == $o->term_full_entity_id ) continue ; // Avoid duplicate items
		$last = $o->term_full_entity_id ;
		$label = $o->term_text ;
		$label = ucfirst ( trim ( preg_replace ( '/\s*\(.*$/' , '' , $label ) ) ) ;
		$q2label[$o->term_full_entity_id] = $label ;
	}
}


print "<textarea style='width:100%' rows='20'>" ;
foreach ( $tmp AS $q => $label ) {
	print "{$q}\tL$missing_lang\t\"$label\"\n" ;
}
print "</textarea>" ;


print get_common_footer() ;

?>