<?PHP

#error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
#ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
require_once ( 'php/wikidata.php' ) ;
header('Content-type: application/json');
#$db = openToolDB ( 'duplicity_p' ) ;
$callback = $_REQUEST['callback'] ;
$out = array () ;
$testing = isset($_REQUEST['testing']) ;

$log_filename = '/data/project/wikidata-todo/public_html/pasleim_projectmerge_game.log' ;

if ( $_REQUEST['action'] == 'desc' ) {
	$out = array (
		"label" => array ( "en" => 'Pasleim project merge' ) ,
		"description" => array ( "en" => "Potential items to be merged, via Pasleim projectmerge." ) ,
		"icon" => 'https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Merge-arrows.svg/100px-Merge-arrows.svg.png' ,
		'options' => array (
#			array ( 'name' => 'Wiki' , 'key' => 'wikitype' , 'values' => array ( 'lang' => 'Your main language' , 'species' => 'WikiSpecies' , 'commons' => 'Wikimedia Commons' ) )
		)
	) ;
} else if ( $_REQUEST['action'] == 'tiles' ) {
	$num = get_request('num',0)*1 ; // Number of games to return
	$lang = get_request('lang','en') ;
//	$wikitype = get_request('wikitype','lang') ;
	$wiki = preg_replace('/[^a-z_-]/','',$lang) . 'wiki' ;
	if ( $wikitype == 'species' ) $wiki = 'specieswiki' ;
	if ( $wikitype == 'commons' ) $wiki = 'commonswiki' ;
	$hadthat_tmp = explode ( ',' , preg_replace ( '/[^0-9,]/' , '' , get_request ( 'in_cache' , '' ) ) ) ;
	$hadthat = array() ;
	foreach ( $hadthat_tmp AS $v ) {
		if ( $v != '' ) $hadthat[] = $v ;
	}
	
	$done = file_get_contents ( $log_filename ) ;
	$done = explode ( "\n" , $done ) ;
	foreach ( $done AS $row ) {
		$row = explode ( "\t" , $row ) ;
		if ( count($row) != 2 ) continue ;
		$hadthat[] = $row[0] ;
	}

	$j = json_decode ( file_get_contents ( '/data/project/wikidata-todo/public_html/pasleim_projectmerge.json' ) ) ;
	$db = openDB ( 'wikidata' , 'wikidata' ) ;

	$out['tiles'] = array() ;
	while ( count($out['tiles']) < $num ) {
		$v = $j[array_rand($j)] ;
		$q1 = 'Q' . $v[0] ;
		$q2 = 'Q' . $v[1] ;
		$id = $q1 . '|' . $q2 ; // Pseudo-ID from both items
		
		if ( in_array ( $id , $hadthat ) ) continue ;
		
		# Check if one of them is redirect, already merged
		$skip = false ;
		$sql = "SELECT * FROM page WHERE page_namespace=0 AND page_title IN ('$q1','$q2') AND page_is_redirect=1" ;
		$result = getSQL ( $db , $sql ) ;
		while($o = $result->fetch_object()) $skip = true ;
		$hadthat[] = $id ;
		if ( $skip ) continue ;
		
		
		$g = array(
			'id' => $id ,
			'sections' => array () ,
			'controls' => array ()
		) ;
		$g['sections'][] = array ( 'type' => 'item' , 'q' => $q1 ) ;
		$g['sections'][] = array ( 'type' => 'item' , 'q' => $q2 ) ;
		$g['controls'][] = array (
			'type' => 'buttons' ,
			'entries' => array (
				array ( 'type' => 'green' , 'decision' => 'yes' , 'label' => 'Same topic' , 'api_action' => array ('action'=>'wbmergeitems','fromid'=>$q2,'toid'=>$q1,'ignoreconflicts'=>'description|sitelink' ) ) ,
				array ( 'type' => 'white' , 'decision' => 'skip' , 'label' => 'Skip' ) ,
				array ( 'type' => 'blue' , 'decision' => 'no' , 'label' => 'Different' )
			)
		) ;
		$out['tiles'][] = $g ;
	}

} else if ( $_REQUEST['action'] == 'log_action' ) {

	$decision = get_request ( 'decision' , '' ) ;
	$id = get_request('tile',-1) ;
	
	if ( $decision != 'skip' and $id != -1 ) {
		$fh = fopen ( $log_filename , 'a' ) ;
		fwrite ( $fh , "$id\t$decision\n" ) ;
		fclose ( $fh ) ;
	}
	
} else {
	$out['error'] = "No valid action!" ;
}

if ( $callback != '' ) print $callback . '(' ;
print json_encode ( $out ) ;
if ( $callback != '' ) print ")\n" ;

?>