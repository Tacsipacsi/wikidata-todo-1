<?PHP

//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
//ini_set('display_errors', 'On');
set_time_limit ( 60 * 15 ) ; // Seconds

require_once ( 'php/common.php' ) ;

$mode = get_request ( 'mode' , 'langlinks' ) ;

$db = openDB ( 'wikidata' , '' ) ;

print get_common_header ( '' , 'Wikidata project stats' ) ;

print "<h3>Modes</h3>" ;
print "<div><a href='?mode=langlinks'>Item links by projects</a> (~1 min)</div>" ;
print "<div><a href='?mode=labels'>Item labels by language</a> (~4 min)</div>" ;

$sql = 'select count(*) AS cnt from page where page_namespace=0' ;
$total_items = 0 ;
$result = getSQL ( $db , $sql ) ;
while($r = $result->fetch_object()){
	$total_items = $r->cnt ;
}
print "<h3>Results</h3>" ;
print "<div>At this moment, Wikidata has a total of " . number_format ( $total_items ) . " items.</div>" ;


print "<table class='table table-condensed table-striped'><thead>" ;

if ( $mode == 'langlinks' ) {
	print "<tr><th>Project</th><th style='text-align:right'>Linked items</th><th style='text-align:right'>% items linked</th><th style='text-align:right'>% items not linked</th></tr>" ;
	$sql = 'select ips_site_id AS project_key,count(*) as cnt from wb_items_per_site group by project_key order by cnt desc' ;
}

if ( $mode == 'labels' ) {
	print "<tr><th>Language</th><th style='text-align:right'>Items with labels</th><th style='text-align:right'>% items with labels</th><th style='text-align:right'>% items without labels</th></tr>" ;
	$sql = 'select term_language AS project_key,count(*) AS cnt from wb_terms where term_type="label" and term_entity_type="item" group by project_key order by cnt desc' ;
}

print '</thead><tbody>' ;

$result = getSQL ( $db , $sql ) ;
while($r = $result->fetch_object()){
	print "<tr><td>" . $r->project_key . "</td>" ;
	print "<td style='text-align:right;font-family:Courier'>" . number_format ( $r->cnt ) . "</td>" ;
	print "<td style='text-align:right;font-family:Courier'>" . number_format ( $r->cnt * 100 / $total_items , 1 ) . "</td>" ;
	print "<td style='text-align:right;font-family:Courier'>" . number_format ( 100 - $r->cnt * 100 / $total_items , 1 ) . "</td>" ;
	print "</tr>" ;
}
print "</tbody></table>" ;

print get_common_footer() ;

?>