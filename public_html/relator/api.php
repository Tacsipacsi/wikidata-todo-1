<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');

//set_time_limit ( 60 * 10 ) ; // 10min
ini_set('memory_limit','500M');

require_once ( '../php/common.php' ) ;

$action = get_request ( 'action' , '' ) ;

$out = [ 'status' => 'OK' , 'data' => [] ] ;

if ( $action == 'random_person' ) {

	$db = openDB ( 'wikidata' , 'wikidata' ) ;

	$max = 0 ;
	$sql = "SELECT max(page_id) AS max FROM page" ;
	$result = getSQL ( $db , $sql ) ;
	$o = $result->fetch_object();
	$max = $o->max ;

	$r = rand(0,$max);
	$sql = "
		SELECT pl_from 
		FROM pagelinks
		WHERE pl_from_namespace=0 
		AND pl_namespace=120
		AND pl_title='P40'
		AND pl_from>=$r
		ORDER BY pl_from
		LIMIT 1
	" ;
	$result = getSQL ( $db , $sql ) ;
	$o = $result->fetch_object();

	$sql = "SELECT page_title FROM page WHERE page_id={$o->pl_from}" ;
	$result = getSQL ( $db , $sql ) ;
	$o = $result->fetch_object();

	$out['data'] = $o->page_title ;
}

header('Content-Type: application/json');
print json_encode ( $out ) ;

?>