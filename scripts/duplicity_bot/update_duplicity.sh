#/bin/bash
cd /data/project/wikidata-todo/scripts/duplicity_bot

\rm wikis
for a in $( curl -s 'https://www.wikidata.org/w/api.php?action=query&meta=siteinfo&siprop=interwikimap&sifilteriw=local&format=json' | jq -r '.query.interwikimap[] | .prefix' | sed 's/-/_/g' ); do echo $a"wiki" >> wikis ; done
cat wikis | xargs -n 1 ./bot.php

./bot.php commonswiki
