#!/usr/bin/php
<?PHP

include_once ( '../public_html/php/common.php' ) ;

function qlink ( $i ) {
	return "<a href='//www.wikidata.org/wiki/Q$i' target='_blank'>Q$i</a>" ;
}

if ( !isset($argv[1]) ) {
	print "Usage: " . $argv[0] . " propnum(,propnum,...) min_cluster_size[10]\n" ;
	exit ( 0 ) ;
}

$props = explode ( ',' , $argv[1] ) ; //array ( 279 ) ;
$minimal_clustersize = $argv[2] ;
if ( !isset($minimal_clustersize) ) $minimal_clustersize = 10 ; // Default
$query2 = '' ;
if ( isset($argv[3]) ) $query2 = ' AND ' . $argv[3] ;

$proplist = array() ;
foreach ( $props AS $p ) $proplist[] = "P$p" ;
$props = implode ( "," , $props ) ;
$proplist = implode ( "," , $proplist ) ;

$url = $wdq_internal_url . "?q=" . urlencode ( "claim[$props]".$query2 ) ;
$json = json_decode ( file_get_contents ( $url ) ) ;

$hadthat = array() ;

$out = array() ;
foreach ( $json->items AS $x => $q ) {
	if ( isset($hadthat[$q]) ) continue ;
	
	$url = $wdq_internal_url . "?q=" . urlencode ( "web[$q][$props]".$query2 ) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	
	$s = count ( $j->items ) ;
	foreach ( $j->items AS $i ) $hadthat[$i] = 1 ;
	if ( $s < $minimal_clustersize ) continue ;
	$one = $j->items[0] ;
	
	$url = $wdq_internal_url . "?q=" . urlencode ( "web[$q][$props] AND noclaim[$props]".$query2 ) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	
	$o = array() ;
	foreach ( $j->items AS $i ) {
		$o[]= qlink($i) ;
	}
	if ( count ( $o ) == 0 ) $o = array ( qlink($one) , '<b>circular web</b>' ) ;

	$o = "<li>" . implode ( '; ' , $o ) ;
	$o .= " (cluster of $s items)" ;
	$o .= "</li>" ;
	$out[] = array ( $o , $s*1 ) ;
//	fwrite ( $fp , $o ) ;
}

function cmp ( $a , $b ) {
	if ( $a[1] == $b[1] ) return 0 ;
	return ( $a[1] < $b[1] ) ? 1 : -1 ;
}

$head = "<p class='lead'>These are \"top-level\" (root) items in clusters of >= " . $minimal_clustersize . " items that need to be linked to the \"$proplist web\".</p>" ;
$head .= "<p>Each entry (row) represents a self-contained cluster of items linked via ($proplist). " ;
$head .= "Each item in a row has no \"parent\" item, and is thus a potential root of that cluster. " ;
$head .= "A \"proper\" tree should have only one root, so multiple roots should be resolved by adding statements of ($proplist) to all but one of these items, to form a proper tree.</p>" ;

$head .= "<p>The first row should contain the main (largest) cluster. Ideally, there should only be only that cluster with a single root. " ;
$head .= "You can add smaller, single-root clusters to the main cluster by adding ($proplist) statements to the root item of the smaller cluster.</p>" ;
$head .= "<p>Clusters that are circular (no root) are marked as such, and one random item of the cluster is linked. To form a proper tree, resolve the circularity by removing ($proplist) statements.</p>" ;




$fp = fopen("../public_html/clusters/$proplist.html", 'w');

fwrite ( $fp , get_common_header('','Clusters of '.$proplist) ) ;
fwrite ( $fp , $head ) ;
fwrite ( $fp , '<ol>' ) ;

usort ( $out , 'cmp' ) ;
foreach ( $out AS $o ) fwrite ( $fp , $o[0] ) ;

fwrite ( $fp , '</ol>' ) ;
fwrite ( get_common_footer() ) ;

fclose ( $fp ) ;

?>