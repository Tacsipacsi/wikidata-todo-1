#!/usr/bin/php
<?php

require_once ( '/data/project/wikidata-todo/public_html/php/ToolforgeCommon.php' ) ;

$tfc = new ToolforgeCommon ( 'wikidata-todo' ) ;
$db = $tfc->openDBtool ( 'wikidata_merge_by_id_p' ) ;

$props_blacklist = [ 'P709' , 'P638' , 'P484' , 'P594' , 'P705' , 'P1566' , 'P2249' ] ;

$props = [] ;
$sparql = 'SELECT ?property WHERE { ?property wikibase:propertyType wikibase:ExternalId ; wdt:P2302 wd:Q21502410 }' ; # Properties with external-id and "distinct value constraint"
$props = $tfc->getSPARQLitems ( $sparql ) ;

foreach ( $props AS $prop ) {
	if ( in_array ( $prop , $props_blacklist) ) continue ;
	$prop_num = preg_replace ( '/\D/' , '' , $prop ) * 1 ;
	$sql = "UPDATE candidates SET `tag`=1 WHERE prop={$prop_num}" ;
	$tfc->getSQL ( $db , $sql ) ;
	$sparql = "SELECT ?q1 ?q2 ?value { ?q1 wdt:{$prop} ?value . ?q2 wdt:{$prop} ?value . FILTER ( ?q1 != ?q2 ) }" ;
	$j = $tfc->getSPARQL ( $sparql ) ;
	foreach ( $j->results->bindings AS $b ) {
		$q1 = preg_replace ( '/\D/' , '' , $tfc->parseItemFromURL ( $b->q1->value ) ) * 1 ;
		$q2 = preg_replace ( '/\D/' , '' , $tfc->parseItemFromURL ( $b->q2->value ) ) * 1 ;
		if ( $q1 > $q2 ) continue ; // ALWAYS Q1<Q2
		$sql = "INSERT INTO candidates (q1,q2,prop,`value`,random,tag) VALUES ({$q1},{$q2},{$prop_num},'".$db->real_escape_string(substr($b->value->value,0,32))."',rand(),0) ON DUPLICATE KEY UPDATE `tag`=0" ;
		$tfc->getSQL ( $db , $sql ) ;
	}
	$sql = "DELETE FROM candidates WHERE prop={$prop_num} AND tag=1" ;
	$tfc->getSQL ( $db , $sql ) ;
}

?>