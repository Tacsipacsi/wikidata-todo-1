#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$ref = "\tS143\tQ13679" ;
#$disam = explode ( "\n" , file_get_contents ( 'disam.tab' ) ) ;

$dbsw = openDB ( 'en' , 'wikispecies' , true ) ;

# Get pages categorized as disambiguation
$disam = getPagesInCategory ( $dbsw , 'Disambiguation pages' , 9 , 0 , true ) ;

# Get "may refer to" pages without categories
$sql = "select * from page WHERE NOT EXISTS (SELECT * FROM page_props WHERE pp_page=page_id AND pp_propname='wikibase_item') AND NOT EXISTS (SELECT * FROM categorylinks WHERE cl_from=page_id) AND page_namespace=0 AND page_is_redirect=0" ;
if(!$result = $dbsw->query($sql)) die('There was an error running the query [' . $dbsw->error . ']'." 1\n$sql\n\n");
while($o = $result->fetch_object()) {
	$title = $o->page_title ;
	$url = "https://species.wikimedia.org/wiki/" . myurlencode($title) ;
	$html = file_get_contents ( $url ) ;
	$html = preg_replace ( '/\s+/' , ' ' , $html ) ;
	if ( preg_match ( '/\bmay refer to\b/' , $html ) ) {
		$disam[$title] = $title ;
	}
}


$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
$fh = fopen ( "disam.add" , 'w' ) ;

foreach ( $disam AS $d ) {

	$d = str_replace ( '_' , ' ' , $d ) ;
	$t = $db->real_escape_string ( $d ) ;
	
	$skip = false ;
	$sql = "SELECT * FROM wb_items_per_site WHERE ips_site_id='specieswiki' AND ips_site_page='$t'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'." 1\n$sql\n\n");
	while($o = $result->fetch_object()) $skip = true ;
	if ( $skip ) continue ;
	
	$targets = array() ;
	$sql = "select distinct epp_entity_id from wb_entity_per_page,pagelinks where epp_entity_type='item' AND epp_page_id=pl_from and pl_namespace=0 and pl_title='Q4167410' AND epp_entity_id IN (select DISTINCT term_entity_id from wb_terms where term_text='$t' and term_entity_type='item' and term_language='en' and term_type in ('label'))" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'." 1\n$sql\n\n");
	while($o = $result->fetch_object()) $targets[] = $o->epp_entity_id ;
	
#	if ( count($targets) > 1 ) { // Wut?
#		print "$d\t" . implode ( ',' , $targets ) . "\n" ;
#		print_r ( $targets ) ;
#		continue ;
#	}
	
	$s = '' ;
	if ( count($targets) == 0 ) { // New one
		$s = "CREATE\n" ;
		$s .= "LAST\tP31\tQ4167410$ref\n" ; # Disambig
		$s .= "LAST\tSspecieswiki\t\"$d\"\n" ;
		$d = preg_replace ( '/ \(.+$/' , '' , $d ) ;
		$s .= "LAST\tLen\t\"$d\"\n" ;
	} else { // Add to existing
		$q = 'Q' . $targets[0] ;
		$s = "$q\tSspecieswiki\t\"$d\"\n" ;
	}

	fwrite ( $fh , $s ) ;
}

fclose ( $fh ) ;

?>