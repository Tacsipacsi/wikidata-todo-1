#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$hadthat = array() ;

$db = openDB ( 'en' , 'wikispecies' , true ) ;
$todo = getPagesInCategory ( $db , 'Taxon Authorities' , 0 , 0 , true ) ;

//$wikis = array ( 'dewiki' , 'enwiki' , 'svwiki' , 'frwiki' , 'ptwiki' , 'euwiki' , 'cawiki' , 'zhwiki' , 'ruwiki' , 'plwiki' , 'itwiki' ) ;
$wikis = array() ;
$sql = "SELECT distinct ll_lang FROM langlinks" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	if ( !preg_match ( '/wik/' , $o->ll_lang ) ) $wikis[] = $o->ll_lang . 'wiki' ;
}

function run ( $wiki ) {
	global $hadthat , $todo ;
	if ( !preg_match ( '/^(.+)wiki$/' , $wiki , $m ) ) return ;
	$lang = $m[1] ;
	
	$wiki_pages = array() ;
	$wiki2sp = array() ;
	$db = openDB ( 'en' , 'wikispecies' , true ) ;
	$sql = "SELECT DISTINCT page_title,ll_title FROM page,langlinks WHERE page_id=ll_from and page_namespace=0 and ll_lang='$lang'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		if ( preg_match ( '/\:/' , $o->ll_title ) ) continue ; // Paranoia
//if ( !isset($todo[$o->page_title]) ) continue ; // Only the ones on the positive list!
		$o->ll_title = str_replace ( '_' , ' ' , $o->ll_title ) ;
		$o->page_title = str_replace ( '_' , ' ' , $o->page_title ) ;
		if ( isset($hadthat[$o->page_title]) ) continue ;
//		if ( $o->ll_title == 'Henri Alain Liogier' ) print "!!\n" ;
		$wiki2sp[$o->ll_title] = $o->page_title ;
		$wiki_pages[] = $db->real_escape_string ( $o->ll_title ) ;
	}

	$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
	$sql = "SELECT ips_item_id,ips_site_page FROM wb_items_per_site WHERE ips_site_id='$wiki' AND ips_site_page IN ('" . implode("','",$wiki_pages) . "')" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$q = 'Q'.$o->ips_item_id ;
		$wiki_page = $o->ips_site_page ;
		if ( !isset($wiki2sp[$wiki_page]) ) {
			print "NOT FOUND: " . $wiki_page . "\n" ;
			continue ;
		}
		$sp_page = $wiki2sp[$wiki_page] ;
//		print "$q\tSspecieswiki\t\"$sp_page\"\n" ;
		$hadthat[$sp_page] = 1 ;
	}
}

$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
$sql = "SELECT DISTINCT ips_site_page FROM wb_items_per_site WHERE ips_site_id='specieswiki'" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$hadthat[$o->ips_site_page] = 1 ;
}

foreach ( $wikis AS $wiki ) run ( $wiki ) ;

?>