#!/usr/bin/php
<?PHP

# !!! RUN find_all_taxa.php FIRST!

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$taxa = explode ( "\n" , file_get_contents ( 'taxa.tab' ) ) ;
$fh = fopen ( "taxa.create" , 'w' ) ;
$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
$ref = "\tS143\tQ13679" ;

foreach ( $taxa AS $t ) {

	if ( preg_match ( '/unspecified/i' , $t ) ) continue ;

	// Check sitelinks
	$sql = "SELECT * FROM wb_items_per_site WHERE ips_site_id='specieswiki' AND ips_site_page='" . $db->real_escape_string($t) . "'" ;
	$skip = false ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'." 1\n$sql\n\n");
	if($o = $result->fetch_object()) $skip = true ;
	if ( $skip ) continue ;
	
	// Check labels and aliases
	$t2 = array() ;
	$t2[] = $db->real_escape_string($t) ;
	if ( preg_match ( '/^(.+) \(/' , $t , $m ) ) $t2[] = $m[1] ;
	$sql = "SELECT * FROM wb_terms WHERE term_entity_type='item' AND term_type IN ('label','alias') AND term_text IN ('" . implode("','",$t2) . "')" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'." 1\n$sql\n\n");
	if($o = $result->fetch_object()) $skip = true ;
	if ( $skip ) continue ;
	
	// Check taxon names
	$items = getSPARQLitems ( "" ) ;
#	$url = "http://wdq.wmflabs.org/api?q=string[225:\"$t\"]" ;
#	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( count($items) > 0 ) continue ;
	

	$s = "CREATE\n" ;
	$s .= "LAST\tSspecieswiki\t\"$t\"\n" ;
	$s .= "LAST\tLen\t\"$t\"\n" ;
	$s .= "LAST\tP31\tQ16521$ref\n" ;
	$s .= "LAST\tP225\t\"$t\"\n" ;
	
	fwrite ( $fh , $s ) ;
}

fclose ( $fh ) ;

?>